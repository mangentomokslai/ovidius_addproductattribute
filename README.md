# README #

This module introduces custom attribute and attribute type for product. Implementation consists of two main aspects:

---

1.Attribute Type Backend Model    - serialization of attribute value for DB storage

2.Attribute Type Renderer         - visual representation (HTML) of attribute in admin panel

---

KeyValue Backend model converts attribute value in array representation (retrieved from renderer) into json string for storage in DB. Atribute value consists of key/value pairs.

Renderer generates inputs on basis of attribute value as represented in DB (json string). First, flat value is converted into object and then formatted into 2D array (key/value pairs). Finally, HTML inputs are generated from aforementioned array representation of attribute value.
