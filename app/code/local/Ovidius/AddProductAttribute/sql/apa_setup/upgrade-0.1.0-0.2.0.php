<?php
$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

$installer->updateAttribute(
    'catalog_product',
    'specs',
    'frontend_model',
    'apa/entity_attribute_frontend_keyvalue'
);

$installer->updateAttribute(
    'catalog_product',
    'specs',
    'is_html_allowed_on_front',
    true
);

$installer->endSetup();
