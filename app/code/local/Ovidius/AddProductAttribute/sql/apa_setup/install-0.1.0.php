<?php
// Resource: https://www.webguys.de/magento-1/eav-attribute-setup

/*
    catalog/setup installer is needed for adding attribute
    In this case attribute meta data is stored in catalog_eav_attribute table instead of eav_attribute.
    Former table extends latter table and is linked with foreign key, eg:
        SELECT * FROM eav_attribute e, catalog_eav_attribute ce
        WHERE e.attribute_id = ce.attribute_id

*/
$installer = Mage::getResourceModel('catalog/setup','catalog_setup');
$installer->startSetup();

$data = array(
    'backend'       => 'apa/entity_attribute_backend_json',
    'class'         => '',
    'default'       => '',
    'frontend'      => '',
    'input_renderer'    => 'apa/catalog_product_helper_form_keyvalue',
    'label'         => 'Specs',
    'required'      => false,
    'source'        => '',
    'type'          => 'varchar',
    'user_defined'  => false,   // ability for user to remove attribute from set
    'unique'        => false,
    'visible_on_front'   => true,
);

/*
    addAttribute params:
        $entityTypeId       entity_type_id or entity_type_code from eav_entity_type table
        $code               attribute_code value for the eav_attribute table
        array $attr         A configuration array for attribute configuration values
*/

$installer->addAttribute('catalog_product', 'specs', $data);
$installer->endSetup();
