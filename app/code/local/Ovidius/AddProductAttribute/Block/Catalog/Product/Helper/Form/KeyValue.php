<?php
class Ovidius_AddProductAttribute_Block_Catalog_Product_Helper_Form_Keyvalue
    extends Varien_Data_Form_Element_Abstract
{
    private $data;

    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('key_value');
        $this->setExtType('key_value');
    }

    protected function _prepareValues()
    {
        $value = $this->getValue();
        $pData = array();

        if ($value != '')
            $data = json_decode($value);
        else
            return array();

        foreach($data->keys as $key => $val) {
            $pData[$val] = $data->values[$key];
        }

        return $pData;
    }

    protected function _getElementJs($data)
    {
        $cnt = count($data);
        $lastIdx = $cnt - 1;
        ob_start(); ?>
        <script>
            Window.keepMultiModalWindow = true;
            var lastIdx = '<?php echo $lastIdx ?>';
            var cnt = '<?php echo $cnt ?>';
            var name = "<?php echo $this->getName() ?>";
            var keyValueRenderer = {
                remove : function(idx, id) {
                    document.getElementById(id +"_"+idx).remove();
                    cnt--;
                    if(cnt == 0) {   // create hidden element indicating empty value
                        var noValueNode = document.createElement("INPUT");
                        noValueNode.id = "no_value_"+name;
                        noValueNode.type = "hidden";
                        noValueNode.name = name;
                        document.getElementById("kvt_"+id).appendChild(noValueNode);
                    }
                },
                add : function(id, name) {
                    lastIdx++;
                    cnt++;
                    var node = document.createElement("TR");
                    node.id = id+"_"+lastIdx;
                    node.innerHTML = "<td><input id=\""
                        +id+"_k_"+lastIdx+"\" name=\""+name
                        +"[keys][]\"></td><td><input id=\""
                        +id+"_v_"+lastIdx+"\" name=\""+name
                        +"[values][]\"></td><button type=\"button\" onclick=\"keyValueRenderer.remove(\'"
                        +lastIdx+"\', \'"+id+"\')\" data-target=\""+id+"_v_"
                        +lastIdx+"\" class=\"kv-remove\">remove</button><td></td>";
                    document.getElementById("kvt_"+id).appendChild(node);

                    if(cnt == 1 && document.getElementById("novalue_"+name)) {
                        document.getElementById("novalue_"+name).remove();
                    }
                }
            }
        </script>;
        <?php return ob_get_clean();
    }

    public function getElementHtml()
    {
        $helper = Mage::helper('core');
        $data = $this->_prepareValues();
        $i = 0;
        $html = '<table id="kvt_'.$this->getHtmlId()
            .'" class="key-value-table"><tr><th></th><th></th><th></th></tr>';
        $cnt = count($data);
        if ($cnt > 0) {
            foreach ($data as $key => $val) {
                $htmlRow = '<tr id="'.$this->getHtmlId().'_'.$i.'"><td>';
                $htmlRow .= '<input id="'.$this->getHtmlId().'_k_'.$i.'" name="'.$this->getName()
                      .'[keys][]" value="'.$key.'" '
                      .$this->serialize($this->getHtmlAttributes()).'/></td>';
                $htmlRow .= '<td><input id="'.$this->getHtmlId().'_v_'.$i.'" name="'.$this->getName()
                      .'[values][]" value="'.$val.'" '
                      .$this->serialize($this->getHtmlAttributes()).'/></td>';
                $htmlRow .= "<td><button type=\"button\" onclick=\"keyValueRenderer.remove('"
                    .$i."', '".$this->getHtmlId()."')\" data-target=\"".$this->getHtmlId()
                    ."._v_".$i.'" class="kv-remove">remove</button></td></tr>';
                $html .= $htmlRow;
                $i++;
            }
        }

        $html .= '</table>';
        $html .= '<tr><td class="label"></td><td class="value"><button onclick="keyValueRenderer.add(\''
            .$this->getHtmlId().'\', \''.$this->getName().'\')" type="button">'
            .$helper->__('Add').'</button></td></td>';
        $html .= $this->_getElementJs($data);
        return $html;
    }
}
