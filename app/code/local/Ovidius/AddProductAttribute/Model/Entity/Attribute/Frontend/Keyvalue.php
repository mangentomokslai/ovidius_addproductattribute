<?php
class Ovidius_AddProductAttribute_Model_Entity_Attribute_Frontend_Keyvalue 
    extends Mage_Eav_Model_Entity_Attribute_Frontend_Abstract
{
    /**
     * Retreive attribute value
     *
     * @param $object
     * @return mixed
     */
    public function getValue(Varien_Object $object)
    {
        $rawValue = $object->getData($this->getAttribute()->getAttributeCode());
        $value = json_decode($rawValue);
        if (!$rawValue || $value == null )
            return '';

        $keys = $value->keys;
        $values = $value->values;
        $html = '<table style="width: 100%">';
        foreach ($keys as $ki => $kv) {
            $html .= '<tr><td><strong>' . $kv . '</strong></td><td>' . $values[$ki] . '</td></tr>';
        }
        $html .= '</table>';

        return $html;
    }
}
