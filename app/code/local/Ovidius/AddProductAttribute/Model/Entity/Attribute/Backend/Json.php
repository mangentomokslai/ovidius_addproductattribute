<?php
class Ovidius_AddProductAttribute_Model_Entity_Attribute_Backend_Json extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * Before save method
     *
     * @param Varien_Object $object
     * @return Mage_Eav_Model_Entity_Attribute_Backend_Abstract
     */
     public function beforeSave($object)
     {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $data = $object->getData($attributeCode);
        if (is_array($data)) {
            $data = array_filter($data);
            $object->setData($attributeCode, json_encode($data));
        }
         
         parent::beforeSave($object);
         return $this;
     }

}
